import Hello from './Hello.js';

function App() {
  return (
    <div>
      <h1>Hello World</h1>
      <h2>Hello {"React"} { 1 + 2 }</h2>
      <Hello text="Testing 123" />
      <Hello text="Hola" />
    </div>
  );
}

export default App;
