// this is our first component

function Hello(props) {
    return (
        <div>
            <p className="test">Hello</p>
            {/* this is inline styling */}
            <p style={{color:'green'}}>
                Lorem ipsum dolar sit amet...
            </p>
            <button onClick={ () => alert('You clicked me!') }>
                { props.text }
            </button>
        </div>
    );
}

export default Hello;