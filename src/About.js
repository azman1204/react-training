// this is classs-based component
import { Component } from 'react';

class About extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {text, msg} = this.props;
        return (
            <p>
                <p>{ text } { msg }</p>
                <strong>Name : John Doe</strong>
                <hr />
                <strong>Email : john@gmail.com</strong>
            </p>
        );
    };
}

export default About;