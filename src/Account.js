import { Component } from 'react';

class Account extends Component {
    state = {  }

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            passwordConfirmation: '',
            email: '',
            errors: []
        }
        this.validateUsername = this.validateUsername.bind(this);
        this.submitForm = this.submitForm.bind(this);
        this.validatePassword = this.validatePassword.bind(this);
        this.validateConfirmationPassword = this.validateConfirmationPassword.bind(this);
        this.validateEmail = this.validateEmail.bind(this);
    }

    submitForm(event) {
        console.log(event);
        console.log(this.state);
    }

    validateUsername(event) {
        console.log(event.target.value);
        let value = event.target.value;
        if (value === '') {
            let errors = this.state.errors;
            errors.push('Username cannot empty');
            //this.setState({errors: errors})
            this.setState({errors});
        }
        this.setState({username: value});
    }

    validatePassword(event) {
        let value = event.target.value;
        this.setState({password: value});
    }

    validateConfirmationPassword(event) {
        let value = event.target.value;
        this.setState({passwordConfirmation: value});
    }

    validateEmail(event) {
        let value = event.target.value;
        this.setState({email: value});
    }

    render() { 
        return (
            <div className="app">
                {
                this.state.errors.map((err) => (
                    <p>{err}</p>
                ))
                }
                User Name: <input type='text' onBlur={this.validateUsername} /> <br />
                Password: <input type='text' onBlur={this.validatePassword} /> <br />
                Password Confirmation: <input type='text' onBlur={this.validateConfirmationPassword} /> <br />
                Email: <input type='text' onBlur={this.validateEmail} /><br />
                <button onClick={this.submitForm}>Submit</button>
            </div>
        );
    }
}
 
export default Account;